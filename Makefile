# Base name of distribution and release files
NAME	=	fabtotum-edition

# Version is read from first paragraph of REAMDE file
VERSION		?=	$(shell grep '^Prusa Edition [0-9]\+\.[0-9]\+.[0-9]\+' README.md README.md | head -n1 | cut -d' ' -f2)
# If no version is found use 0.<date>.<time> generated version
ifeq ($(VERSION),)
VERSION = 0.$(shell date +%Y%m%d.%H%M)
endif

# Priority for colibri bundle
PRIORITY	?= 902

# FABUI license
LICENSE		?= GPLv2

# OS flavour identifier
OS_FLAVOUR	?= colibri

# System paths
LIB_PATH		 ?= /var/lib/$(NAME)
SHARED_PATH		 ?= /usr/share/$(NAME)
COLIBRI_LIB_PATH ?= /var/lib/colibri
METADATA_PATH	 ?= $(COLIBRI_LIB_PATH)/bundle/$(NAME)
WWW_PATH		 ?= /var/www
MOUNT_BASE_PATH	 ?= /mnt
APP_PATH		 ?= $(SHARED_PATH)
LOG_PATH		 ?= /var/log/$(NAME)

TEMP_PATH	 	 ?= /tmp
BIGTEMP_PATH	 ?= $(MOUNT_BASE_PATH)/bigtemp
USERDATA_PATH	 ?= $(MOUNT_BASE_PATH)/userdata
DB_PATH			 ?= $(LIB_PATH)
USB_MEDIA_PATH	 ?= /run/media
LOCALE_PATH		 ?= $(APP_PATH)/locale

WEB_FRAMEWORK_PATH ?= application/

# OS paths
PHP_CONFIG_FILE_SCANDIR ?= /etc/php/conf.d/
CRON_FOLDER 			?= /var/spool/cron/crontabs/

# Build/Install paths
BUILD_DIR 		= ./build
BDATA_DIR 		= $(BUILD_DIR)/bdata
BDATA_STAMP		= $(BUILD_DIR)/.bdata_stamp
APP_BUNDLE		= $(PRIORITY)-$(NAME)-v$(VERSION).cb
APP_BUNDLE_GLOB	= $(PRIORITY)-$(NAME)-v*.cb

DESTDIR 		?= $(BDATA_DIR)

OS_FILES_DIR	= ./os

# This is not a mistake. OS_STAMP is general dependency used by bundle rule
OS_STAMP		= $(BUILD_DIR)/.os_$(OS_FLAVOUR)_stamp
# OS_COLIBRI_STAMP is specific stamp used in case OS_FLAVOUR is colibri
OS_COLIBRI_STAMP= $(BUILD_DIR)/.os_colibri_stamp
OS_COMMON_STAMP	= $(BUILD_DIR)/.os_common_stamp

# User/group
WWW_DATA_NAME	= www-data
WWW_DATA_UID 	= 33
WWW_DATA_GID 	= 33
WWW_DATA_GROUPS	= wheel,dialout,tty,plugdev,video

# Tools
INSTALL			?= install
FAKEROOT 		?= fakeroot
FAKEROOT_ENV 	?= $(FAKEROOT) -s $(BUILD_DIR)/.fakeroot_env -i $(BUILD_DIR)/.fakeroot_env --
MKSQUASHFS		?= mksquashfs
BUNDLE_COMP		?= lzo

########################### Makefile rules #############################
.PHONY: locale clean distclean bundle check-tools docs \
	install-application \
	install-os-common \
	install-os-colibri

all: check-tools $(APP_BUNDLE)

application/node_modules:
	cd application && npm install

clean:
	rm -rf $(BUILD_DIR)
	[ -n $(CONFIG_FILES) ] && rm -rf $(CONFIG_FILES)
	[ -n $(DB_FILES) ] && rm -rf $(DB_FILES)

distclean: clean
	rm -rf *.cb
	rm -rf *.cb.md5sum
	[ -n $(GENERATED_FILES) ] && rm -f $(GENERATED_FILES)

check-tools:
	@which fakeroot > /dev/null
	@echo "Looking for fakeroot: FOUND"
	
	@which mksquashfs > /dev/null
	@echo "Looking for mksquashfs: FOUND"

install: install-app install-os-common install-os-$(OS_FLAVOUR)
	echo "Install to $(DESTDIR)"
	
bundle: check-tools distclean $(APP_BUNDLE)

# $(BUILD_DIR):
# 	mkdir -p $@

$(DESTDIR):
	mkdir -p $@

$(APP_BUNDLE): $(BDATA_DIR) install-app install-os-common install-os-$(OS_FLAVOUR)
	rm -f $(APP_BUNDLE_GLOB) $(APP_BUNDLE_GLOB).md5sum
	$(FAKEROOT_ENV) $(MKSQUASHFS) $(BDATA_DIR) $@ -noappend -comp $(BUNDLE_COMP) -b 512K -no-xattrs
	md5sum $@ > $@.md5sum

install-app:  $(DESTDIR) $(DB_FILES) $(GENERATED_FILES)
#	Add metadata
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/$(METADATA_PATH)
#	metadata/info
	$(FAKEROOT_ENV) echo "name: $(NAME)" > $(DESTDIR)/$(METADATA_PATH)/info
	$(FAKEROOT_ENV) echo "version: $(VERSION)" >> $(DESTDIR)/$(METADATA_PATH)/info
	$(FAKEROOT_ENV) echo "build-date: $(shell date +%Y-%m-%d)" >> $(DESTDIR)/$(METADATA_PATH)/info
#	metadata/packages
	$(FAKEROOT_ENV) echo "$(NAME): $(VERSION)" > $(DESTDIR)/$(METADATA_PATH)/packages
#	metadata/licenses
	$(FAKEROOT_ENV) echo "$(NAME): $(LICENSE)" > $(DESTDIR)/$(METADATA_PATH)/licenses
#	license files
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/usr/share/licenses/$(NAME)
	$(FAKEROOT_ENV) cp LICENSE $(DESTDIR)/usr/share/licenses/$(NAME)
#   Install bundle helper scripts
	$(FAKEROOT_ENV) cp -R $(OS_FILES_DIR)/colibri/meta/* $(DESTDIR)/$(METADATA_PATH)/
########################################################################
#	Application data
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/etc/fabui
	$(FAKEROOT_ENV) install -D -m 0644 $(OS_FILES_DIR)/colibri/etc/fabui/feeds.ini $(DESTDIR)/etc/fabui/feeds.ini
# 	Fix permissions
#	$(FAKEROOT_ENV) chown -R $(WWW_DATA_UID):$(WWW_DATA_GID) $(DESTDIR)$(WWW_PATH)

	
# install-os-common:
# # 	Avahi service
# 	$(FAKEROOT_ENV) install -d -m 0775 $(DESTDIR)/etc/default
# # CRON file
# 	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)$(CRON_FOLDER)

install-os-colibri:
	$(FAKEROOT_ENV) mkdir -p $(DESTDIR)/etc/colibri/sources.d
	$(FAKEROOT_ENV) install -D -m 0644 $(OS_FILES_DIR)/colibri/sources.d/os_stable.conf \
		$(DESTDIR)/etc/colibri/sources.d

	$(FAKEROOT_ENV) install -d -m 0775 $(DESTDIR)/etc/default
	$(FAKEROOT_ENV) install -D -m 0644 $(OS_FILES_DIR)/colibri/etc/default/fabui-services $(DESTDIR)/etc/default/fabui-services

#include blueprint.mk